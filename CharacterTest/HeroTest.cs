using Xunit;

namespace CharacterTest
{
    public class HeroTest
    {
        [Fact]
        public void HeroConstructor_HeroCreation_HeroLevelOne()
        {
            Warrior warrior = new Warrior(); 
            int expected = 1;
            int realValue = warrior.Level;
            Assert.Equal(expected, realValue);
        }
        [Fact]
        public void Constructor_LevelUp_HeroLevelTwo()
        {
            Warrior warrior = new Warrior();
            warrior.LevelUp();
            int expected = 2;
            int realValue = warrior.Level;
            Assert.Equal(expected, realValue);
        }
        [Fact]
        public void Constructor_HeroesInit_WarriorCorrectAttributes()
        {
            Warrior warrior = new Warrior();
            
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1,
            };
            PrimaryAttribute realValue = warrior.TotalAttributes;
            Assert.Equal(expected.Strength, realValue.Strength);
            Assert.Equal(expected.Dexterity, realValue.Dexterity);
            Assert.Equal(expected.Intelligence, realValue.Intelligence);
        }
        [Fact]
        public void Constructor_HeroesInit_RogueCorrectAttributes()
        {
            Rogue Rogue = new Rogue();

            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1,
            };
            PrimaryAttribute realValue = Rogue.TotalAttributes;
            Assert.Equal(expected.Strength, realValue.Strength);
            Assert.Equal(expected.Dexterity, realValue.Dexterity);
            Assert.Equal(expected.Intelligence, realValue.Intelligence);
        }
        [Fact]
        public void Constructor_HeroesInit_RangerCorrectAttributes()
        {
            Ranger ranger = new Ranger();

            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1,
            };
            PrimaryAttribute realValue = ranger.TotalAttributes;
            Assert.Equal(expected.Strength, realValue.Strength);
            Assert.Equal(expected.Dexterity, realValue.Dexterity);
            Assert.Equal(expected.Intelligence, realValue.Intelligence);
        }
        [Fact]
        public void Constructor_HeroesInit_MageCorrectAttributes()
        {
            Mage mage = new Mage();

            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
            };
            PrimaryAttribute realValue = mage.TotalAttributes;
            Assert.Equal(expected.Strength, realValue.Strength);
            Assert.Equal(expected.Dexterity, realValue.Dexterity);
            Assert.Equal(expected.Intelligence, realValue.Intelligence);
        }

        //-------------------------------------------
        [Fact]
        public void Constructor_HeroesLevelUp_WarriorCorrectAttributes()
        {
            Warrior warrior = new Warrior();
            warrior.LevelUp();
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 8,
                Dexterity = 4,
                Intelligence = 2,
            };
            PrimaryAttribute realValue = warrior.TotalAttributes;
            Assert.Equal(expected.Strength, realValue.Strength);
            Assert.Equal(expected.Dexterity, realValue.Dexterity);
            Assert.Equal(expected.Intelligence, realValue.Intelligence);
        }
        [Fact]
        public void Constructor_HeroesLevelUp_RogueCorrectAttributes()
        {
            Rogue Rogue = new Rogue();
            Rogue.LevelUp();
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 3,
                Dexterity = 10,
                Intelligence = 2,
            };
            PrimaryAttribute realValue = Rogue.TotalAttributes;
            Assert.Equal(expected.Strength, realValue.Strength);
            Assert.Equal(expected.Dexterity, realValue.Dexterity);
            Assert.Equal(expected.Intelligence, realValue.Intelligence);
        }
        [Fact]
        public void Constructor_HeroesLevelUp_RangerCorrectAttributes()
        {
            Ranger ranger = new Ranger();
            ranger.LevelUp();
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 2,
                Dexterity = 12,
                Intelligence = 2,
            };
            PrimaryAttribute realValue = ranger.TotalAttributes;
            Assert.Equal(expected.Strength, realValue.Strength);
            Assert.Equal(expected.Dexterity, realValue.Dexterity);
            Assert.Equal(expected.Intelligence, realValue.Intelligence);
        }
        [Fact]
        public void Constructor_HeroesLevelUp_MageCorrectAttributes()
        {
            Mage mage = new Mage();
            mage.LevelUp();
            PrimaryAttribute expected = new PrimaryAttribute()
            {
                Strength = 2,
                Dexterity = 2,
                Intelligence = 13,
            };
            PrimaryAttribute realValue = mage.TotalAttributes;
            Assert.Equal(expected.Strength, realValue.Strength);
            Assert.Equal(expected.Dexterity, realValue.Dexterity);
            Assert.Equal(expected.Intelligence, realValue.Intelligence);
        }
    }
}