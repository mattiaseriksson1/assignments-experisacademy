﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;




namespace CharacterTest
{
    public class EquipmentTest
    {
        Weapon testAxe = new Weapon()
        {
            Name = "Common axe",
            RequiredLevel = 1,
            ItemSlot = Item.ItemSlots.WeaponSlot,
            WeaponType = WeaponType.Axe,
            WeaponAttribute = new WeaponAttribute()
            { 
             Damage = 7, 
             AttackSpeed = 1.1 
            }
        };
        Armor testPlateBody = new Armor()
        {
            Name = "Common plate body armor",
            RequiredLevel = 1,
            ItemSlot = Item.ItemSlots.BodySlot,
            ArmorType = ArmorType.Plate,
            PrimaryAttribute = new PrimaryAttribute() 
            {
                Strength = 1 
            }
        };
        Weapon testBow = new Weapon()
        {
            Name = "Common bow",
            RequiredLevel = 1,
            ItemSlot = Item.ItemSlots.WeaponSlot,
            WeaponType = WeaponType.Bow,
            WeaponAttribute = new WeaponAttribute() 
            {
                Damage = 12,
                AttackSpeed = 0.8 
            }
        };
        Armor testClothHead = new Armor()
        {
            Name = "Common cloth head armor",
            RequiredLevel = 1,
          
            ItemSlot = Item.ItemSlots.HeadSlot,
            
           


            ArmorType = ArmorType.Cloth,
            PrimaryAttribute = new PrimaryAttribute() 
            { 
                Intelligence = 5 
            }
        };

        [Fact]
        public void EquipWeapon_WeaponLevelTooHigh_RequiredLevelError()
        {
            Warrior warrior = new Warrior();
            testAxe.RequiredLevel = 2;
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testAxe));
            
        }
        [Fact]
        public void EquipArmor_ArmorLevelTooHigh_RequiredLevelError()
        {
            Warrior warrior = new Warrior();
            testPlateBody.RequiredLevel = 2;
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testPlateBody));
        }
        [Fact]
        public void EquipWeapon_WrongWeaponType_WeaponTypeError()
        {
            Warrior warrior = new Warrior();
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testBow));
        }
        [Fact]
        public void EquipArmor_WrongArmorType_ArmorTypeError()
        {
            Warrior warrior = new Warrior();
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testClothHead));
        }
        [Fact]
        public void EquipWeapon_WeaponEquippedSuccess_WeaponEquippedMessage()
        {
            Warrior warrior = new Warrior();
           
            string expected = "New weapon equipped!";
            Assert.Equal(expected, warrior.EquipWeapon(testAxe));
        }
        [Fact]
        public void EquipArmor_ArmorEquippedSuccess_ArmorEquippedMessage()
        {
            Warrior warrior = new Warrior();
            string expected = "New armour equipped!";
            Assert.Equal(expected, warrior.EquipArmor(testPlateBody));
        }
        [Fact]
        public void AttackTest_NoWeaponAttack_GetDamage()
        {
            Warrior warrior = new Warrior();
            int expected = 1 * (1 + (5 / 100));
            Assert.Equal(expected, warrior.Attack());
        }
        [Fact]
        public void AttackTest_ValidWeaponAndNoArmorAttack_GetDamage()
        {
            Warrior warrior = new Warrior();
            warrior.EquipWeapon(testAxe);
            double expected = (7 * 1.1) * (1 + (5 / 100));
            Assert.Equal(expected, warrior.Attack());
        }
        [Fact]
        
        public void AttackTest_ValidWeaponAndArmorAttack_GetDamage()
        {
        Warrior warrior = new Warrior();
        warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testPlateBody);
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            Assert.Equal(expected, warrior.Attack());
        }
             
    }
}
