global using Xunit;
global using RPG.equipment;
global using RPG.heroes;

global using RPG.stats;
global using RPG;
global using RPG.exceptions;