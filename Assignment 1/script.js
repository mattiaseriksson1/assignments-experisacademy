let bankLoanBtn; //Button that lets the user take a bank loan
let salary; //HTML-element used for the salary.
let earnMoney; //Button under the Work-section of the website. Clicking this button will add 100 Kr to the salary
let outstandingLoan; //The loan 
let existingLoan = false; //Boolean to make sure if there is an existing loan registered to the user.
let loanAmount = 0; //The amount of money loaned from the bank. 
let bankBalance; //HTML-element used for the bank balance under the Bank-section of the website.
let originalBankBalance = 0; //Starting value in the bank balance. This variable is used to add funds to the bankBalance-element.
let transferBtn; //Button that lets a user transfer his salary to his bank balance. 
let buyNowBtn; //Button that lets a user buy a laptop.
let boughtComputers = []; //Array for the bought laptops, once a computer is bought, it is added to the array. To make sure that you cant buy the same laptop several times
let salaryAmount = 0; //Starting value for the salary. Used among other things to add the money that is earned by working into the salary. 

/*This function is called when windows load. The purpose is mainly to call the three main functions
of this code (work, Bank and fetchLaptop). */
function init(){
    work()
    bank()
    fetchLaptop();
    buyNowBtn = document.getElementById("BuyNow");
    bankBalance = document.getElementById("bankBalance");
    bankBalance.innerHTML =  Number(originalBankBalance);
}
/*This is the main function of the bank.
Its main purpose is to display the current balance and let the user take a loan depending on the balance-amount */
function bank(){
    outstandingLoan = document.getElementById("outstandingLoan");
    bankLoanBtn = document.getElementById("bankLoanBtn");
    bankLoanBtn.addEventListener("click", function(){
    let maxLoan = Number(bankBalance.innerHTML) * 2; // Users can´t get a loan higher than twice the bank-balance, this variable checks what the max-loan is and then makes sure that they dont surpass that.
    loanAmount = prompt("Loan amount? Max loan: " + maxLoan );  
        if( Number(loanAmount) > Number(maxLoan) && typeof(loanAmount) != Number){ 
            alert("No Loan! You are not allowed to loan more than " + maxLoan);
        }
        else if(existingLoan == true){
            alert("You can only have one outstanding loan at the time");
        }
        else if(0 < Number(loanAmount) < Number(maxLoan)) {
            alert("Loan Approved! Amount: " + loanAmount  )
            existingLoan = true;
            outstandingLoan.innerHTML = Number(loanAmount) ;
            originalBankBalance += Number(loanAmount);
            bankBalance.innerHTML =  Number(originalBankBalance);
            payLoan();
        }
    })  
}
/* This is the main function of the work-section.
It will let the user earn money by clicking the earn-money-Button/Work to earn 100 kr for each click.
It will also append a button if there is an existing loan, that is used to pay back the loan (payLoan).
Or the user could just transfer the earned money to the bank (transferToBank) */
function work(){   
    salary = document.getElementById("salary");
    earnMoney = document.getElementById("earnMoney");
    earnMoney.addEventListener("click", function(){
        salaryAmount += Number(100);
        salary.innerHTML = Number(salaryAmount);
        bankBalance.innerHTML = Number(originalBankBalance)
    })
    transferBtn = document.getElementById("transferToBank");
    transferBtn.addEventListener("click", transferToBank);
        if(loanAmount > 0){ //Creates a button for paying back the loan if the outstanding loan is higher than 0.
    
            let payBackBtn = document.createElement("button");
            payBackBtn.innerHTML = "Pay Back Loan";
            document.getElementById("payBack").append(payBackBtn)
        }
}
/*The PayLoan function is activated once a user presses the "Pay Back Loan Button". 
The purpose is to pay back the loan using the money existing in salary.innerHTML.
Then after the loan is fully paid off, it will remove the pay-loan button. */
function payLoan (){
    if(loanAmount > 0){ 
        let payBackBtn = document.createElement("button"); //The button is created when a loan has been taken. The purpose is to let the user pay back their loan
        payBackBtn.innerHTML = "Pay Back Loan";
        payBackBtn.id = "payBackBtn";
        document.getElementById("payBack").append(payBackBtn)
        payBackBtn.addEventListener("click", function(){
            outstandingLoan.innerHTML = Number(outstandingLoan.innerHTML);
            salary.innerHTML = Number(salary.innerHTML); 
                    if(Number(salary.innerHTML) > Number(outstandingLoan.innerHTML)){              
                        salary.innerHTML = salary.innerHTML - outstandingLoan.innerHTML;
                            if(outstandingLoan.innerHTML < 0){
                                salary.innerHTML = Math.abs(outstandingLoan.innerHTML);
                
                             }
                        salaryAmount = Number(0) + Number(salary.innerHTML);
                        outstandingLoan.innerHTML = 0 ;
                        existingLoan = false;
                        payBackBtn.parentNode.removeChild(payBackBtn);
                    }
                    else if(salary.innerHTML == outstandingLoan.innerHTML){ 
                        outstandingLoan.innerHTML = outstandingLoan.innerHTML - salary.innerHTML;
                        salaryAmount = 0;
                        salary.innerHTML = 0;
                        existingLoan = false;
                        payBackBtn.parentNode.removeChild(payBackBtn);
                    }
                    else if(salary.innerHTML < outstandingLoan.innerHTML){ 
                        outstandingLoan.innerHTML = outstandingLoan.innerHTML - salary.innerHTML;
                        salary.innerHTML = 0;
                        salaryAmount = 0;
                    }
            return;
        })
    }
    else if(loanAmount == 0){
        payBackBtn.parentNode.removeChild(payBackBtn);
    }
}
/*This functions purpose is to transfer the money from salary to the bank balance.
How much of the money in the salary that gets transferred to the bank balance depends on if there 
is an existing loan. If there is a loan, only 90% of the salary goes into the bank balance, the rest 
goes to the loan, otherwise all of the salary will go to the bank balance. */
function transferToBank(){
    if(existingLoan == false && Number(salary.innerHTML) > 0){ //No loan, The entire salary goes to the bank-balance
        originalBankBalance += Number(salary.innerHTML);
        bankBalance.innerText = Number(originalBankBalance);
        salaryAmount = 0;
        salary.innerHTML = salaryAmount;
    }
    else if(existingLoan == true && Number(salary.innerHTML) > 0){ //If there is an existing loan, pay off 10% of the loan when transferring to the bank
        originalBankBalance += Number(salary.innerHTML) * 0.9;
        bankBalance.innerText = Number(originalBankBalance);
        outstandingLoan.innerHTML = Number(outstandingLoan.innerHTML) - (Number(salary.innerHTML) * 0.1);
        salaryAmount = 0;
        salary.innerHTML = salaryAmount;
        if(Number(outstandingLoan.innerHTML) <= 0){
            console.log(document.getElementById("payBackBtn").parentNode.removeChild(document.getElementById("payBackBtn")));
            originalBankBalance += Math.abs(outstandingLoan.innerHTML);
            outstandingLoan.innerHTML = 0;
            existingLoan = false;   
        }
    }
}
/*This function fetches the laptops from the API */
async function fetchLaptop(){
    try {
        const response = await fetch("https://noroff-komputer-store-api.herokuapp.com/computers");
        const laptops = await response.json();
        laptops[4].image = laptops[4].image.replace(".jpg", ".png"); //Changes the image on the computer "The visor" from a JPG to PNG.
        displayLaptop(laptops);
    }
    catch(error){
        console.log(error);
    }
    finally{
    }
}
/*Add an option for all laptops in the select-menu.
Then checks which laptop that has been selected and then displays all the information for that laptop */ 
function displayLaptop(laptops){
    let laptopSelectList = document.getElementById("laptopChoices"); //Select-list for all the different laptops.
    let laptopInformation = document.getElementById("laptopInformation"); //ul-list to store all laptop specs.
    let laptopName = document.getElementById("laptopName"); //Laptop Name
    let laptopImg = document.getElementById("laptopImg"); //Laptop Image
    let laptopDesc = document.getElementById("laptopDesc"); //Laptop Description
    let laptopPrice = document.getElementById("laptopPrice"); //Laptop Price
        for (let i=0; i<laptops.length; i++){ //Goes through all laptops in the API and adds them to the select-list
            let laptopOption = document.createElement("OPTION"); //list item with laptops
            laptopOption.id = laptops[i].id;
            laptopOption.innerHTML = laptops[i].title;
            laptopSelectList.appendChild(laptopOption);
        }
    laptopSelectList.addEventListener("change", function(){
    laptopInformation.innerHTML = "";
    let thisLaptop = ""; //thisLaptop is the chosen laptop.
    thisLaptop = laptops[laptopSelectList.selectedIndex - 1] 
        for(let i=0; i< thisLaptop.specs.length; i++){ //goes through all the specs of a laptop and displays them
            let listItem = document.createElement("li");
            listItem.innerHTML = thisLaptop.specs[i];
            laptopInformation.appendChild(listItem);
        } 
    laptopName.innerHTML = thisLaptop.title;
    laptopDesc.innerHTML = thisLaptop.description;
    laptopPrice.innerHTML = `Pris:  ${thisLaptop.price} Kr` ;
    laptopImg.src = "https://noroff-komputer-store-api.herokuapp.com/" + thisLaptop.image;
    middleman(laptopSelectList.selectedIndex, thisLaptop);
    })
}
/*This function acts like a middleman between displayLaptops and BuyLaptop. Perhaps not needed but it works
Makes sure that the correct laptop is the one being displayed and bought, also checks if the laptop hasnt already been bought before */
function middleman(index, laptop){
    if(index != 0  ){ //Makes sure that the value selected isn´t "choose Laptop"
        buyNowBtn.disabled = false; 
        buyNowBtn.addEventListener("click",function(){
            if(laptop.title == laptopName.innerHTML && boughtComputers.includes(laptop.title) == false){  
                buyLaptop(laptop); 
            }   
        })
    }
    else{
    buyNowBtn.disabled = true
    }
}
/*This function lets you buy the laptop. If the user confirms the purchase, it then checks if there is enough
money in the bank balance. If there is enough, the money will be drawn from the account, the laptop will be added to the boughtComputers array, and an alert will confirm the purchase.
If there isn´t enough money, then an alert will let the user know that there are insufficient funds.*/ 
function buyLaptop(laptop){
    let confirmScreen = confirm("Are you sure that you want to buy " + laptop.title + "?");
        if(confirmScreen == true){
            if(bankBalance.innerHTML >= laptop.price){ //Checks if the user has enough money
                bankBalance.innerHTML = originalBankBalance - laptop.price;
                originalBankBalance = originalBankBalance - laptop.price;
                boughtComputers.push(laptop.title);
                alert("Congratulations! You are now the proud owner of " + laptop.title + ". NO REFUNDS");
            }
            else if (laptop.price > bankBalance.innerHTML){
                alert("Insufficient funds. The transaction did not go through.")
            }
        }
        else{
            return;
        }
}
window.onload = init; //calls init-function when windows load. 
