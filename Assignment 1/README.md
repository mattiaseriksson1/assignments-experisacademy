**Komputer Store**

This assignment featured three parts: 
- The Bank, users can see the money in their bank-account and then get a bank-loan
- Work, users can earn money by working and transferring the money to the bank or pay off a bank-loan
- The computer Store, users can browse different computers and then purchase one.


Tools: 
- JavaScript (Without frameworks)
- CSS
- HTML

