﻿using System.Text;
using System;
using System.Threading.Tasks;

namespace RPG.exceptions
{
    /// <summary>
    /// Exception for invalid armor
    /// Called when the hero is trying to equip armor with a too high required level or the wrong armor-type.
    /// </summary>
    /// 
    public class InvalidArmorException : Exception
    {

        public InvalidArmorException(string ErrorMessage) : base(ErrorMessage)
        {

            

        }
     
    }

}
