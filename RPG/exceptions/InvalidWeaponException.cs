﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.exceptions
{
    /// <summary>
    /// Exception for invalid Weapons
    /// Called when the hero is trying to equip a weapon with a too high required level or a weapon which the class is not able to use.
    /// </summary>
    
    public class InvalidWeaponException: Exception
    {

    public  InvalidWeaponException(string ErrorMessage): base(ErrorMessage)
    {
            
     

    }
       
    }
    
}
