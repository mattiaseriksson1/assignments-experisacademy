﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.equipment
{
    /// <summary>
    /// The abstract for Items.
    /// </summary>
    public abstract class Item 
    {
        public string Name { get; set; } //Name of Item
        public int RequiredLevel { get; set; } //The required Level to use the item
        
        public ItemSlots ItemSlot { get; set; } //Item slots
        public enum ItemSlots
        {
            HeadSlot,
            BodySlot,
            LegSlot,
            WeaponSlot
        }
       

        public Dictionary<ItemSlots, Item> Equipment = new Dictionary<ItemSlots, Item>
        {
                { ItemSlots.HeadSlot, null },
                { ItemSlots.BodySlot, null },
                { ItemSlots.LegSlot, null },
                { ItemSlots.WeaponSlot, null }
            };
    }
}
