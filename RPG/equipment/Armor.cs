﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.stats;
using RPG.heroes;

namespace RPG.equipment
{
    /// <summary>
    /// Enum with all different Armor-types
    /// </summary>
     public enum ArmorType
    {
        Cloth, 
        Leather,
        Mail,
        Plate
    }
    /// <summary>
    /// Constructor for the armor. Extends the Item-class, with Armor-type and primaryAttributes (Strength, Dexterity, and Intelligence).
    /// </summary>
    public class Armor: Item
    {
        public ArmorType ArmorType { get; set; } //Type of Armor, Which material
        public PrimaryAttribute PrimaryAttribute { get; set; } //The attributes of the armor.
        
        
    }
    
}
