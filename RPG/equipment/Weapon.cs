﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.equipment;
using RPG.stats;
using RPG.heroes;

namespace RPG.equipment
{
    /// <summary>
    /// Enum for the different types of weapons
    /// </summary>
    public enum WeaponType
    {
        Axe, 
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }
    /// <summary>
    /// Class for the weapons, extends the Item-class with The Weapontype-enum and Weaponattributes(Damage and AttackSpeed)
    /// </summary>
    public class Weapon: Item
    {
    public WeaponType WeaponType { get; set; } //Type of Weapon, like Bows or Swords
    public WeaponAttribute WeaponAttribute { get; set; } //Damage and attackspeed

    /// <summary>
    /// DamagePerSecond returns a double with the DPS(Damage Per Second) by multiplying the weapons attackspeed and Weapons damage.
    /// </summary>
    /// <returns></returns>
    public double DamagePerSecond()
        {
          
            double DPS = WeaponAttribute.AttackSpeed * WeaponAttribute.Damage;
       
            return DPS;
        }

    }
    
}
