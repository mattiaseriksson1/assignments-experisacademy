﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.heroes;

namespace RPG.stats
{
    /// <summary>
    /// hero/equipment attributes
    /// </summary>
    public class PrimaryAttribute 
    {

        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        
    }
  
    
}
        
    

