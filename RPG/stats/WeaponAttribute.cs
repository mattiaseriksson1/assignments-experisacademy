﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.stats
{
    /// <summary>
    /// Attributes for weapons
    /// </summary>
    public class WeaponAttribute
    {
        public double Damage { get; set; }
        public double AttackSpeed { get; set; }
        public double DPS { get; set; }

    }
}
