﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.stats;
using RPG.equipment;


namespace RPG.heroes
{
    /// <summary>
    /// the abstract for Hero.
    /// Contains all the stats for heroes.
    /// </summary>
    /// 
    public abstract class Hero
    {
        public string HeroClass { get; set; } //Which class the hero is.
        public string Name { get; set; } //Name of hero

        public int Level { get; set; } = 1; //When created, the user will start at level 1.
        public double HeroDamage { get; set; } = 1;
        //public PrimaryAttribute BaseAttribute { get; set; }

        public PrimaryAttribute totalAttribute { get; set; } //The attributes of the hero

        public ItemSlots ItemSlot {get; set;}
        public enum ItemSlots
        {
            HeadSlot,
            BodySlot,
            LegSlot,
            WeaponSlot
        }
        public Dictionary<ItemSlots, Item> Equipment = new Dictionary<ItemSlots, Item>
        {
                { ItemSlots.HeadSlot, null },
                { ItemSlots.BodySlot, null },
                { ItemSlots.LegSlot, null },
                { ItemSlots.WeaponSlot, null }
            };


        /// <summary>
        /// A method featuring a stringbuilder to display all the details about a hero.
        /// </summary>
        /// 
        /// <param name="totalAttributes">The total attributes of a hero, base attributes + equipment attributes</param>
        public void DisplayHero(PrimaryAttribute totalAttributes)//
        {
            StringBuilder s = new StringBuilder();
            s.Append("Class: " + HeroClass + "\n");
            s.Append("Name: " +Name + "\n");
            s.Append("Level: " + Level + "\n");
            s.Append("strength: " + totalAttributes.Strength + "\n");
            s.Append("Dexterity: " + totalAttributes.Dexterity + "\n");
            s.Append("Intelligence: " + totalAttributes.Intelligence + "\n");
            s.Append("Damage: " + HeroDamage + "\n");
            Console.WriteLine(s);
            
        }
       
      
    }
}
