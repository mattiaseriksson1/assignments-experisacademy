﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.stats;
using RPG.equipment;
using RPG.exceptions;

namespace RPG.heroes
{
    /// <summary>
    /// The Rogue class that is extended by Hero. 
    /// Rogue contains the primary Attributes of the class.
    /// </summary>
    public class Rogue: Hero
    {
   
        public PrimaryAttribute TotalAttributes = new PrimaryAttribute()
        {
            Strength = 2,
            Dexterity = 6,
            Intelligence = 1,
        };

        /// <summary>
        /// The Level up-method.
        /// It will create a new PrimaryAttribute object with the level-up values and add them to the hero Total attributes.
        /// It will also update the level of the hero. 
        /// </summary>
        public void LevelUp()
        {
            PrimaryAttribute newValues = new PrimaryAttribute()
            {
                Strength = 1,
                Dexterity = 4,
                Intelligence = 1,

            };
            TotalAttributes.Strength += newValues.Strength;
            TotalAttributes.Dexterity += newValues.Dexterity;
            TotalAttributes.Intelligence += newValues.Intelligence;
            Level += 1;


        }
        /// <summary>
        /// Method for equipping a weapon. EquipWeapon checks if the weapon is the right type and makes sure that the required level isnt higher than the hero-level.
        /// </summary>
        /// <param name="Weapon">The Weapon the hero is trying to equip</param>
        /// <returns></returns>
        /// <exception cref="InvalidWeaponException">If required level is too high or the weapon-type is invalid, it will throw an error to let the user know about the issue</exception>
        public string EquipWeapon(Weapon Weapon)
        {

            if (Weapon.WeaponType != WeaponType.Axe && Weapon.WeaponType != WeaponType.Hammer && Weapon.WeaponType != WeaponType.Sword)
            {
                throw new InvalidWeaponException("This class is not able to use this weapon");
            }
            if (Weapon.RequiredLevel > Level)
            {
                
                throw new InvalidWeaponException("This Weapon requires a higher Hero-level");
            }
            //ItemSlots.Weapon = Weapon;
            else
            {
            Equipment[ItemSlots.WeaponSlot] = Weapon;
            double DPS = Weapon.DamagePerSecond();
            Attack(DPS);
            return "New weapon equipped!";
            }
            

        }
        /// <summary>
        /// Method for equipping a piece of armor. Checks if the armor is the right type and makes sure that the hero level isnt lower than the required level
        /// </summary>
        /// <param name="armor">The piece of armor that the user is trying to equip</param>
        /// <returns></returns>
        /// <exception cref="InvalidArmorException">If the required level is too high or the armor-type is invalid, this exception will be thrown.</exception>
        public string EquipArmor(Armor armor)
        {
            if (armor.ArmorType != ArmorType.Leather && armor.ArmorType != ArmorType.Mail)
            {
                throw new InvalidArmorException("This class is not able to use this armor-type");
            }
            if (armor.RequiredLevel > Level)
            {
                throw new InvalidArmorException("This armor requires a higher Hero-Level");
            }
            else
            {
            Equipment[ItemSlots.BodySlot] = armor;
            TotalAttributes.Strength += armor.PrimaryAttribute.Strength;
            TotalAttributes.Dexterity += armor.PrimaryAttribute.Dexterity;
            TotalAttributes.Intelligence += armor.PrimaryAttribute.Intelligence;

            return "New armour equipped!";
            }
            
        }
        /// <summary>
        /// The method to get the damage. If there is a weapon equipped, it will call the Attack-method with a DPS parameter.
        /// </summary>
        /// <param name="DPS">The Damage per second. </param>
        /// <returns></returns>
        public double Attack(double DPS)
        {

            double Damage = DPS * (1 + TotalAttributes.Dexterity / 100);
            HeroDamage = Damage;
            return Damage;

        }
        /// <summary>
        /// The method for getting the damage if there is no weapon equipped.
        /// </summary>
        /// <returns></returns>
        public double Attack()
        {
            double Damage = HeroDamage * (1 + TotalAttributes.Dexterity / 100);
            HeroDamage = Damage;
            return Damage;
        }
    }
}
